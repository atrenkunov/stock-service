package ru.reboot;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import ru.reboot.controller.StockController;
import ru.reboot.controller.StockControllerImpl;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;
import ru.reboot.service.StockServiceImpl;

import java.util.List;

public class AppTest {

    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void mockTest() {

        List<String> mockList = Mockito.mock(List.class);
        Mockito.when(mockList.get(10)).thenReturn("Hello");

        Assert.assertEquals("Hello", mockList.get(10));
        Assert.assertNull(mockList.get(11));

        Mockito.verify(mockList).get(10);
        Mockito.verify(mockList).get(11);
    }

    @Test
    public void mockRepositoryTest() {

        // prepare
        StockRepository repositoryMock = Mockito.mock(StockRepository.class);
        Mockito.when(repositoryMock.findItem(Mockito.anyString())).thenReturn(new Item());

        StockServiceImpl service = new StockServiceImpl();
        service.setStockRepository(repositoryMock);

        // act
//        service.someMethod(....

        // verify
//        Assert.assertEquals(...
//        Assert.assertEquals(...
    }
}
