package ru.reboot.dto;

public class Item {

    private String id;
    private String name;
    private String short_name;
    private String description;
    private String unit;
    private double purchase_price;
    private String category;
    private String manufacturer;
    private int count;
    private int reserved;
}
