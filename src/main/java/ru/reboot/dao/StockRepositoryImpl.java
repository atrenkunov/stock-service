package ru.reboot.dao;

import ru.reboot.dto.Item;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.util.List;

/**
 * Stock repository.
 */
public class StockRepositoryImpl implements StockRepository {

    private final Connection connection;

    public StockRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Item findItem(String itemId) {
        return null;
    }

    @Override
    public Item findAllItemsByCategory(String category) {
        return null;
    }

    @Override
    public void deleteItem(String itemId) {

    }

    @Override
    public Item createItem(Item item) {
        return null;
    }

    @Override
    public Item updateItem(Item item) {
        return null;
    }

    @Override
    public List<Item> getAllItems() {
        return null;
    }

    @Override
    public List<String> getAllCategories() {
        return null;
    }

    @PreDestroy
    public void onClose() {
        try {
            connection.close();
        } catch (Exception ex) {
        }
    }
}
