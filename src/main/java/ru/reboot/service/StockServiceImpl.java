package ru.reboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reboot.dao.StockRepository;
import ru.reboot.dto.Item;

import java.util.List;

@Component
public class StockServiceImpl implements StockService {

    private StockRepository stockRepository;

    @Autowired
    public void setStockRepository(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public Item getItem(String itemId) {
        return null;
    }

    @Override
    public List<Item> getAllItems() {
        return null;
    }

    @Override
    public List<String> getAllCategories() {
        return null;
    }

    @Override
    public Item reserveItem(String itemId, int count) {
        return null;
    }

    @Override
    public Item unreserveItem(String itemId, int count) {
        return null;
    }

    @Override
    public Item getAllItemsByCategory(String category) {
        return null;
    }

    @Override
    public void deleteItem(String itemId) {
    }

    @Override
    public Item createItem(Item item) {
        return null;
    }

    @Override
    public Item updateItem(Item item) {
        return null;
    }
}
