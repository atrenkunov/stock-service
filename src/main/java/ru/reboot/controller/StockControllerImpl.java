package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.reboot.dao.StockRepositoryImpl;
import ru.reboot.dto.Item;
import ru.reboot.service.StockService;

import java.util.Date;
import java.util.List;

/**
 * Stock controller.
 */
@RestController
@RequestMapping(path = "stock")
public class StockControllerImpl implements StockController {

    private static final Logger logger = LogManager.getLogger(StockRepositoryImpl.class);

    private StockService stockService;

    @Autowired
    public void setStockService(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "StockController " + new Date();
    }

    @Override
    public Item getItem(String itemId) {
        return stockService.getItem(itemId);
    }

    @Override
    public Item getAllItemsByCategory(String category) {
        return stockService.getAllItemsByCategory(category);
    }

    @Override
    public void deleteItem(String itemId) {
        stockService.deleteItem(itemId);
    }

    @Override
    public Item createItem(Item item) {
        return stockService.createItem(item);
    }

    @Override
    public Item updateItem(Item item) {
        return stockService.updateItem(item);
    }

    @Override
    public List<Item> getAllItems() {
        return stockService.getAllItems();
    }

    @Override
    public List<String> getAllCategories() {
        return stockService.getAllCategories();
    }

    @Override
    public Item reserveItem(String itemId, int count) {
        return stockService.reserveItem(itemId, count);
    }

    @Override
    public Item unreserveItem(String itemId, int count) {
        return stockService.unreserveItem(itemId, count);
    }
}
