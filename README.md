# Сервис stock-service

Данный сервис обеспечивает работу с товарами, которые доступны в SMarket.

Ответственный за сервис: Кремнева Наталья

## План работ:

#### Создать таблицу item. Написать liquibase скрипт. Данная таблица хранит товары, которые расположены на складе.

Поля таблицы item

  * id: string
  * name: string
  * short_name: string
  * description: string
  * unit: string
  * purchase_price: String   // закупочная цена
  * category: String
  * manufacturer: String     // производитель
  * count: int
  * reserved: int

### Создать соответствующий класс Item.

### Реализовать rest-контроллер /stock/**/*, который обеспечивает работу с товарами

План работы:

Кремнева Наталья
- Написать ликубейс скрипты и разместить из в папке sql, написать структуру Item, getters & setters
- Получить информацию о товаре GET /stock/item?itemId={itemId}

Никита Кителев
- Получить товары в определенной категории GET /stock/item/all/byCategory?categoryId={categoryId}
- Удалить информацию о товаре DELETE /stock/item?itemId={itemId}

Светлана Лапина
- Создать новый товар POST /stock/item
- Редактировать существующий товар PUT /stock/item

Евгений Деревянка
- Забронировать определенное количество товара POST /stock/reserve?itemId={itemId}&count={count}
- Получить все категории товаров GET /stock/сategory/all

Максим Ефремов
- Отменить бронь определенного количества товара POST /stock/unreserve?itemId={itemId}&count={count}
- Получить все товары GET /stock/item/all

Критерии приемки:
- методы сервиса должны быть протестированы.